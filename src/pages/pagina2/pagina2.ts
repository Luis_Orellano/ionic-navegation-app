import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import { Pagina3Page } from "../index.paginas";
@Component({
  selector: "page-pagina2",
  templateUrl: "pagina2.html",
})
export class Pagina2Page {
  pagina3: any = Pagina3Page;
  mutantes: any[] = [
    {
      nombre: "Magneto",
      poder: "Controlar metales",
      img: "https://www.geekmi.news/__export/1617555272341/sites/debate/img/2021/04/04/magneto1.jpg_374738812.jpg",
    },
    {
      nombre: "Spider-man",
      poder: "Super fuerza e instinto aracnido",
      img: "https://elfarolcultural.com/wp-content/uploads/2020/09/spider-man-1876543-800x445.jpg",
    },
    {
      nombre: "Tormenta",
      poder: "Controlar el clima",
      img: "https://i.ytimg.com/vi/aDT3CsXNvtY/maxresdefault.jpg",
    },
    {
      nombre: "Wolverine",
      poder: "Regeneración acelerada",
      img: "https://e.rpp-noticias.io/normal/2020/04/27/345634_933540.jpg",
    },
    {
      nombre: "Gambito",
      poder: "Cargar objetos inanimados con energia",
      img: "https://e.rpp-noticias.io/normal/2020/07/09/241224_968403.jpg",
    },
    {
      nombre: "Profesor X",
      poder: "Poderes psíquicos",
      img: "https://sm.ign.com/t/ign_latam/screenshot/default/carlosjavier_54aq.1280.jpg",
    },
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  irPagina3(mutante: any) {
    this.navCtrl.push(Pagina3Page, { mutante: mutante });
  }
}
